module.exports = function(app) {
  app.dataSources.mongodb_connection.automigrate('Reservation', function(err) {
    if (err) throw err;
    app.models.Reservation.create([
      {
        'name': 'Karaoke Birthday Party',
        'description': 'Wanna join me on my birthday? We’re going to sing our hearts out!',
        'guests': 16,
        'attending': 10,
        'table': 8,
        'event': 'https://facebook.com/events/SOMEID',
        'date': '2017-11-19T04:17:20.929Z',
        'time': '8:30 PM',
        'service': 'How about some nice balloons everywhere?',
        'user': 'hyuchiadiego'
      },
      {
        'name': 'Bachelor Party',
        'description': 'Wanna join me on my bachelor\'s party?',
        'guests': 8,
        'attending': 8,
        'table': 9,
        'event': 'https://facebook.com/events/SOMEID',
        'date': '2017-12-28T04:17:20.929Z',
        'time': '8:30 PM',
        'service': 'Some people is coming with their children, could there be some juice?',
        'user': 'hyuchiadiego'
      }
    ], function(err, item) {
      if (err) throw err;
      console.log('Models Created:\n', item);
    });
  });
};
