module.exports = function(app) {
  app.dataSources.mongodb_connection.automigrate('Song', function(err) {
    if (err) throw err;
    app.models.Song.create([
      {
        'name': 'Shape of You',
        'artist': 'Ed Sheeran',
        'album': '÷ (Deluxe)',
        'image': 'https://i.scdn.co/image/e6a84983ed9b0a04ce633b021329f7df034e7c7c',
        'url': 'spotify:track:7qiZfU4dY1lWllzX7mPBI3',
        'user': 'hyuchiadiego',
        'picture': 'https://profile-images.scdn.co/images/userprofile/default/50afa07a14c02f3d558232cde7942e9ee7d26d07',
        'date': '2017-11-19T00:00:00.000Z',
        'duration': 233712
      },
      {
        'name': 'Coming Home - Part II',
        'artist': 'Skylar Grey',
        'album': 'The Buried Sessions Of Skylar Grey',
        'image': 'https://i.scdn.co/image/41f5e16cc08a993c165a0f332e07c705a16034b9',
        'url': 'spotify:track:0raNny4W8ri5cAmemwKzSI',
        'user': 'hyuchiadiego',
        'picture': 'https://profile-images.scdn.co/images/userprofile/default/50afa07a14c02f3d558232cde7942e9ee7d26d07',
        'date': '2017-11-19T00:00:00.000Z',
        'duration': 164826
      }
    ], function(err, item) {
      if (err) throw err;
      console.log('Models Created:\n', item);
    });
  });
};
