module.exports = function(app) {
  app.dataSources.mongodb_connection.automigrate('MenuItem', function(err) {
    if (err) throw err;
    app.models.MenuItem.create([
      {
        'name': 'Manhatthan',
        'description': 'Cocktail made with whiskey, sweet vermouth and bitters',
        'price': 22.00,
        'type': 'Drink'
      },
      {
        'name': 'BBQ Wings',
        'description': 'Wings with our traditional spicy BBQ sauce',
        'price': 32.00,
        'type': 'Food'
      },
      {
        'name': 'Chips',
        'description': 'Fried chips with enough sauce to wake you up',
        'price': 12.00,
        'type': 'Snack'
      }
    ], function(err, item) {
      if (err) throw err;
      console.log('Models Created:\n', item);
    });
  });
};
